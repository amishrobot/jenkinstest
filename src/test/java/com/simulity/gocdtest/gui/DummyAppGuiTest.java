/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simulity.gocdtest.gui;

import com.simulity.gocdtest.DummyApp;
import org.assertj.swing.fixture.FrameFixture;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author scodav01
 */
public class DummyAppGuiTest {
    

    @Test
    public void testGui(){
        DummyApp app = new DummyApp();
        FrameFixture f = new FrameFixture(app);
        
        f.textBox("hexInputField").enterText("A");
        f.button("conversionButton").click();
        
        String expected = "10";
        String actual = app.getResult();
        
        assertEquals(expected, actual);
        
    }

}
