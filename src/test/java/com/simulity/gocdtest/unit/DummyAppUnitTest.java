package com.simulity.gocdtest.unit;

import com.simulity.gocdtest.DummyApp;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author scodav01
 */
public class DummyAppUnitTest {
    
    @Test
    public void testHexToInt(){
        int expected = 10;
        int actual = DummyApp.hexToDecimal("A");
        
        assertEquals(expected, actual);
    }
}
