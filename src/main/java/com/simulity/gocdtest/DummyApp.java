/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simulity.gocdtest;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author scodav01
 */
public class DummyApp extends JFrame {
    
    private JPanel fieldsPanel;
    private JPanel buttonPanel;
    private JLabel hexLabel;
    private JTextField hexTF;
    private JLabel decimalLabel;
    private JTextField decimalTF;
    private JButton convertButton;
    
    public DummyApp(){
        initComponents();
        buildGUI();
    }
    
    private void initComponents(){
        fieldsPanel = new JPanel();
        fieldsPanel.setLayout(new GridLayout(2,2));
        
        buttonPanel = new JPanel();
        
        hexLabel = new JLabel("Hex: ");
        hexTF = new JTextField();
        hexTF.setName("hexInputField");
        
        decimalLabel = new JLabel("Decimal: ");
        decimalTF = new JTextField();

        convertButton = new JButton("Convert");
        convertButton.setName("conversionButton");
        convertButton.addActionListener(new ButtonListener());
    }
    
    private void buildGUI(){
        fieldsPanel.add(hexLabel);
        fieldsPanel.add(hexTF);
        fieldsPanel.add(decimalLabel);
        fieldsPanel.add(decimalTF);
        
        buttonPanel.add(convertButton);
        
        this.setLayout(new BorderLayout());
        this.add(fieldsPanel, BorderLayout.CENTER);
        this.add(buttonPanel, BorderLayout.SOUTH);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }
    
    public String getResult(){
        return decimalTF.getText();
    }
    
    public static int hexToDecimal(String hex){
        return Integer.parseInt(hex, 16);
    }
    
    public static void main(String[] args){
        DummyApp app = new DummyApp();
        
    }
    
    private class ButtonListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            decimalTF.setText(String.valueOf(hexToDecimal(hexTF.getText())));
        }
        
    }
}


